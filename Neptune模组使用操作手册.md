# Neptune 

## 介绍

本文档介绍Neptune模组的开发环境搭建、版本编译构建、烧录、源码修改、调试验证等方法。通过学习，开发者会对Neptune模组开发流程有初步认识，并可上手业务开发。

## 参考网址

```
W800官网：http://www.winnermicro.com/html/1/156/158/558.html
鸿蒙官网：https://device.harmonyos.com/cn/home
润和HiHope社区：https://gitee.com/hihopeorg
```

## 代码下载

```
git clone git@gitee.com:hihopeorg/Neptune-HarmonyOS-IOT.git
```

## 编译

### 编译工具链

工具包名：csky-elfabiv2-tools-x86_64-minilibc-xxxxx.tar.gz

下载地址：https://occ.t-head.cn/community/download

选择 工具 - 工具链-800 Series 中相应的版本

下载完成后将工具包拷贝到相应的目录下，如/opt

 解压后，配置环境变量

 操作命令：

```
cp csky-elfabiv2-tools-x86_64-minilibc-xxxxx.tar.gz /opt
tar xzvf csky-elfabiv2-tools-x86_64-minilibc-xxxxx.tar.gz
```

在~/.bashrc文件的最后一行添加

```
export PATH=/opt/bin:$PATH
```

###### 备注：如若没有配置过WiFIOT环境，请先配置如下环境

```
# python包安装
apt-get install -y --no-install-recommends python3 python3-distutils python3-pip \
&& ln -s /usr/bin/python3 /usr/bin/python

# 下载gn/ninja包
URL_PREFIX=https://repo.huaweicloud.com/harmonyos/compiler
DOWNLOAD_DIR=~/Downloads                # 下载目录，可自行修改
TOOLCHAIN_DIR=~/harmonyos/toolchain     # 工具链存放目录，可自行修改

[ -e $DOWNLOAD_DIR ] || mkdir $DOWNLOAD_DIR
[ -e $TOOLCHAIN_DIR ] || mkdir -p $TOOLCHAIN_DIR
wget -P $DOWNLOAD_DIR $URL_PREFIX/gn/1523/linux/gn.1523.tar
wget -P $DOWNLOAD_DIR $URL_PREFIX/ninja/1.9.0/linux/ninja.1.9.0.tar

# 解压gn/ninja/包：
tar -C $TOOLCHAIN_DIR/ -xvf $DOWNLOAD_DIR/gn.1523.tar
tar -C $TOOLCHAIN_DIR/ -xvf $DOWNLOAD_DIR/ninja.1.9.0.tar

# 向 ~/.bashrc 中追加gn/ninja路径配置：
cat <<EOF >> ~/.bashrc
TOOLCHAIN_DIR=$TOOLCHAIN_DIR
export PATH=\$TOOLCHAIN_DIR/gn:\$PATH
export PATH=\$TOOLCHAIN_DIR/ninja:\$PATH
export PATH=~/.local/bin:\$PATH       # 用户pip二进制工具目录
EOF

# 生效环境变量
source ~/.bashrc
```

### 编译命令

```
python build.py w800
```

## 烧写镜像

## 烧写工具

工具包名：SecureCRSecureFXPortable.tar

下载地址：https://gitee.com/hihopeorg/neptune_docs

工具使用：

解压安装后，打开工具 选择connect - new session - serial - 下一步 - 选择对应的 com 口、波特率设置为 115200

注意要取消掉 RTS/CTS 的勾选

### 烧写方式

在PC端按住ESC，主板按下RST键，工具界面出现ccccc时，点击菜单栏中[Transfer]，选择[Send Xmodem]，然后选择要烧录的版本即可。

烧写成功后，工具界面依然出现ccccc，这时按下RST键，系统启动。

备注：详细操作方式请参考[SecureCRT使用.docx](https://gitee.com/qianqian_song/neptune_docs/commit/4f9fed06bd5986d5e3d58122cf598e0ec39ee901)

​          w800.img 烧录镜像

​           w800.fls 裸板镜像

## 用例调试

系统启动后，可以看到开发板上的led在闪烁，按下USER按键，可以改变灯的状态。

用例源码路径：

`applications/sample/wifi-iot/app/othardware/gpio_example.c`

使用GPIO接口输出高低电平，实现控制LED灯亮
设置GPIO引脚方向GpioSetDir
使用GPIO接口注册中断处理函数，实现USER按键响应（输入）
设置内部上拉下拉状态IoSetPull（没有外部上拉电阻时需要设置上拉，才能保证能够检测到下降沿）
注册中断处理函数GpioRegisterIsrFunc，具体参数使用参考头文件说明
具体使用参考wifiiot_gpio.h和wifiiot_gpio_ex.h文件里的注释
