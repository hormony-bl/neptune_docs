## 购买渠道

官方淘宝店铺：【润和芯片社区HopeRun】HiSpark开发套件购买链接：

| 产品                                                         | 链接                                             |
| ------------------------------------------------------------ | ------------------------------------------------ |
| ① HarmonyOS HiSpark AI Camera开发套件 （Hi3516DV300）        | https://item.taobao.com/item.htm?id=622922688823 |
| ② HarmonyOS HiSpark IPC DIY开发套件 （Hi3518EV300）          | https://item.taobao.com/item.htm?id=623376454933 |
| ③ HarmonyOS HiSpark Wi-Fi IoT智能家居开发套件 （Hi3861V100） | https://item.taobao.com/item.htm?id=622343426064 |
| ④ Neptune HarmonyOS 物联网 IOT模组                           | https://item.taobao.com/item.htm?id=635868903111 |
